/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iostream>
using std::cout;
using std::endl;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	size_t byte_num=0, line_num=0, word_num=0;
	size_t width = 0, maxwidth = 0;
	enum states {nws, ws};
	bool state=ws, prevstate=ws;
	string word = "";
	set<string> uwords;
	while(fread(&c, 1, 1, stdin)){
		state = isspace(c);
		if(state==nws) {
			word+=c;
			width++;
		}
		else{
			if(c=='\n'){
				line_num++;
				if(width>maxwidth)maxwidth = width;
				width = 0;
			}
			else if(c=='\t')width += 8 - (byte_num - line_num)%8;
			else width++;
			if(prevstate==nws) {
				uwords.insert(word);
				word_num++;
				word = "";
			}
		}
		byte_num++;
		prevstate = state;
	}
	if(c!='\n'){
		if(width>maxwidth)maxwidth = width;
		line_num++;
		uwords.insert(word);
		word_num++;
		word="";
	}
	if(!(charonly || linesonly || wordsonly || uwordsonly || longonly)){
		cout<<'\t'<<line_num<<'\t'<<word_num<<'\t'<<byte_num;
	}
	if(linesonly)cout << line_num << '\t';
	if(wordsonly)cout << word_num << '\t';
	if(charonly)cout << byte_num << '\t';
	if(longonly)cout << maxwidth << '\t';
	if(uwordsonly)cout << uwords.size() << '\t';
	cout << endl;
	return 0;
}
